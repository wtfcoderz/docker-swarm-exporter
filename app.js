const http = require('http');

const httpGet = path => {
  return new Promise((resolve, reject) => {
    http.get({socketPath: '/var/run/docker.sock', path: path}, res => {
      res.setEncoding('utf8');
      let body = '';
      res.on('data', chunk => body += chunk);
      res.on('end', () => resolve(body));
    }).on('error', reject);
  });
};

http.createServer(function (req, res) {
  let promises = [
    httpGet('/swarm'),
    httpGet('/nodes'),
    httpGet('/services'),
    httpGet('/tasks'),
    httpGet('/info'),
    httpGet('/containers/json?all=1'),
    httpGet('/images/json?all=1'),
    httpGet('/networks'),
    httpGet('/volumes'),
    httpGet('/secrets'),
    httpGet('/configs'),
    httpGet('/plugins')
  ];

  let metrics = {};

  Promise.all(promises)
  .then(function(values) {
    let swarm = JSON.parse(values[0]);
    let nodes = JSON.parse(values[1]);
    let services = JSON.parse(values[2]);
    let tasks = JSON.parse(values[3]);
    let info = JSON.parse(values[4]);
    let containers = JSON.parse(values[5]);
    let images = JSON.parse(values[6]);
    let networks = JSON.parse(values[7]);
    let volumes = JSON.parse(values[8]);
    let secrets = JSON.parse(values[9]);
    let configs = JSON.parse(values[10]);
    let plugins = JSON.parse(values[11]);

    metrics = addSwarmMetrics(metrics, swarm);
    metrics = addNodesMetrics(metrics, swarm, nodes);
    metrics = addInfoMetrics(metrics, info);
    metrics = addServicesMetrics(metrics, swarm, services, tasks);
    //metrics = addPluginMetrics(metrics, plugins);

    let rawData = getRawData(metrics);
    res.write(rawData.join('\n'));
    res.end();

  }).catch(function (err) {
    res.write('Error!');
    console.log(err);
    res.end();
  });

}).listen(8080);

function addMetric(metrics, name, help, type) {
  metrics[name] = metrics[name] || {
    "help": help,
    "type": type,
    "rows": []
  };
  return metrics;
}

function addRow(metrics, name, row) {
  metrics[name].rows.push(row);
  return metrics;
}

function addSwarmMetrics(metrics, swarm) {
  /*{
    "ID": "vyv4b6oef6a1u228dkwxg24hs",
    "Version": {
        "Index": 116
    },
    "CreatedAt": "2018-06-04T09:05:38.20874851Z",
    "UpdatedAt": "2018-06-11T21:11:17.62732017Z",
    "Spec": {
        "Name": "default",
        "Labels": {},
        "Orchestration": {
            "TaskHistoryRetentionLimit": 5
        },
        "Raft": {
            "SnapshotInterval": 10000,
            "KeepOldSnapshots": 0,
            "LogEntriesForSlowFollowers": 500,
            "ElectionTick": 10,
            "HeartbeatTick": 1
        },
        "Dispatcher": {
            "HeartbeatPeriod": 5000000000
        },
        "CAConfig": {
            "NodeCertExpiry": 7776000000000000
        },
        "TaskDefaults": {},
        "EncryptionConfig": {
            "AutoLockManagers": false
        }
    },
    "TLSInfo": {
        "TrustRoot": "-----BEGIN CERTIFICATE-----\nMIIBaTCCARCgAwIBAgIUANNiKN4eOFUPGnMVJWLlNKXA/IIwCgYIKoZIzj0EAwIw\nEzERMA8GA1UEAxMIc3dhcm0tY2EwHhcNMTgwNjA0MDkwMTAwWhcNMzgwNTMwMDkw\nMTAwWjATMREwDwYDVQQDEwhzd2FybS1jYTBZMBMGByqGSM49AgEGCCqGSM49AwEH\nA0IABJgWrUgjltCQgLKIkAlSAxwhmD30kjNZKXOEDzzmIlVeXqtaYPPE4srYPuV1\nRXEnRojVTJ7/7M3lmzNsnU0rs5yjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMB\nAf8EBTADAQH/MB0GA1UdDgQWBBQYgSDqA19N/6qFKlsRwWp3Hz86BjAKBggqhkjO\nPQQDAgNHADBEAiAtv86pFso6U4PSSrRJIpYoo7xJY20HZG5Kfp9QhDBSyQIgfACe\nBYOculV2bDurS7IJqYG+nFFDwI5LhoR50h4gwho=\n-----END CERTIFICATE-----\n",
        "CertIssuerSubject": "MBMxETAPBgNVBAMTCHN3YXJtLWNh",
        "CertIssuerPublicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEmBatSCOW0JCAsoiQCVIDHCGYPfSSM1kpc4QPPOYiVV5eq1pg88Tiytg+5XVFcSdGiNVMnv/szeWbM2ydTSuznA=="
    },
    "RootRotationInProgress": false,
    "JoinTokens": {
        "Worker": "SWMTKN-1-638bp00dwnx9fhqghoxy1v9vw2l3vu2bzullqab57bq8clgvqp-90b7x1i7d78omatl9dt3959rv",
        "Manager": "SWMTKN-1-638bp00dwnx9fhqghoxy1v9vw2l3vu2bzullqab57bq8clgvqp-9xhbe0rkxtnu4zld28zeymuop"
    }
}*/

  return metrics;
}

function addNodesMetrics(metrics, swarm, nodes) {
  // Swarm ID
  let swarmID = swarm.ID;

  let managers = nodes.filter(elem => elem.Spec.Role === "manager");
  let workers = nodes.filter(elem => elem.Spec.Role === "worker");
  let reachableManagers = managers.filter(elem => elem.ManagerStatus.Reachability == "reachable");
  let readyWorkers = workers.filter(elem => elem.Status.State == "ready");

  metrics = addMetric(metrics, "docker_info_nodes_managers_count", "Nodes with manager role.", "counter");
  metrics = addRow(metrics, "docker_info_nodes_managers_count", {
    "labels": {},
    "value": managers.length
  });

  metrics = addMetric(metrics, "docker_info_nodes_reachable_swarm_managers_count", "Reachable Swarm Managers.", "counter");
  metrics = addRow(metrics, "docker_info_nodes_reachable_swarm_managers_count", {
    "labels": {},
    "value": reachableManagers.length
  });

  metrics = addMetric(metrics, "docker_info_nodes_workers_count", "Nodes with worker role.", "counter");
  metrics = addRow(metrics, "docker_info_nodes_workers_count", {
    "labels": {},
    "value": workers.length
  });

  metrics = addMetric(metrics, "docker_info_nodes_ready_swarm_workers_count", "Ready Swarm Workers.", "counter");
  metrics = addRow(metrics, "docker_info_nodes_ready_swarm_workers_count", {
    "labels": {},
    "value": readyWorkers.length
  });

  for (let n in nodes) {
    let node = nodes[n];
    metrics = addMetric(metrics, "docker_info_nodes_resources_nanocpu", "Nano Cpu.", "counter");
    metrics = addRow(metrics, "docker_info_nodes_resources_nanocpu", {
      "labels": {
        "swarm_id": swarmID,
        "node_id": node.ID,
        "node_role": node.Spec.Role,
        "node_hostname": node.Description.Hostname,
        "node_architecture": node.Description.Platform.Architecture,
        "node_operating_system": node.Description.Platform.OS,
        "node_engine_version": node.Description.Engine.EngineVersion
      },
      "value": node.Description.Resources.NanoCPUs
    });
    metrics = addMetric(metrics, "docker_info_nodes_resources_memory_bytes", "MemoryBytes.", "counter");
    metrics = addRow(metrics, "docker_info_nodes_resources_memory_bytes", {
      "labels": {
        "swarm_id": swarmID,
        "node_id": node.ID,
        "node_role": node.Spec.Role,
        "node_hostname": node.Description.Hostname,
        "node_architecture": node.Description.Platform.Architecture,
        "node_operating_system": node.Description.Platform.OS,
        "node_engine_version": node.Description.Engine.EngineVersion
      },
      "value": node.Description.Resources.MemoryBytes
    });
  }

  return metrics;
}

function addInfoMetrics(metrics, info) {
  metrics = addMetric(metrics, "docker_info_global_count", "General counters.", "counter");
  metrics = addRow(metrics, "docker_info_global_count", {
    "labels": {
      "type": "containers"
    },
    "value": info.Containers
  });
  metrics = addRow(metrics, "docker_info_global_count", {
    "labels": {
      "type": "running_containers"
    },
    "value": info.ContainersRunning
  });
  metrics = addRow(metrics, "docker_info_global_count", {
    "labels": {
      "type": "paused_containers"
    },
    "value": info.ContainersPaused
  });
  metrics = addRow(metrics, "docker_info_global_count", {
    "labels": {
      "type": "stopped_containers"
    },
    "value": info.ContainersStopped
  });
  metrics = addRow(metrics, "docker_info_global_count", {
    "labels": {
      "type": "images"
    },
    "value": info.Images
  });
  metrics = addRow(metrics, "docker_info_global_count", {
    "labels": {
      "type": "swarm_nodes"
    },
    "value": info.Swarm.Nodes
  });
  metrics = addRow(metrics, "docker_info_global_count", {
    "labels": {
      "type": "swarm_managers"
    },
    "value": info.Swarm.Managers
  });

  return metrics;
}

function addServicesMetrics(metrics, swarm, services, tasks) {
  let swarmID = swarm.ID;
  metrics = addMetric(metrics, "docker_services_count", "Number of services.", "counter");
  metrics = addRow(metrics, "docker_services_count", {
    "labels": {},
    "value": services.length
  });

  for (let service in services) {
    let s = services[service];
    let metricLabels = {...{swarm_id: swarm.ID, "service_name": s.Spec.Name}, ...prefixKeys("docker_service_label_", s.Spec.Labels)};
    // Get service tasks
    let serviceTasks = tasks.filter(elem => s.ID === elem.ServiceID);
    let serviceTasksDesired = tasks.filter(elem => s.ID === elem.ServiceID && (elem.DesiredState === "running" || elem.DesiredState === "ready"));
    let serviceTasksRunning = tasks.filter(elem => s.ID === elem.ServiceID && elem.Status.State === "running");
    let serviceReservedMemory = serviceTasksDesired
                              .filter(e => undefined != e.Spec.Resources &&
                                           undefined != e.Spec.Resources.Reservations &&
                                           undefined != e.Spec.Resources.Reservations.MemoryBytes)
                              .reduce((a, b) => a + b.Spec.Resources.Reservations.MemoryBytes, 0);
    let serviceReservedNanoCpu = serviceTasksDesired
                              .filter(e => undefined != e.Spec.Resources &&
                                           undefined != e.Spec.Resources.Reservations &&
                                           undefined != e.Spec.Resources.Reservations.NanoCPUs)
                              .reduce((a, b) => a + b.Spec.Resources.Reservations.NanoCPUs, 0);
    let serviceLimitMemory = serviceTasksDesired
                              .filter(e => undefined != e.Spec.Resources &&
                                           undefined != e.Spec.Resources.Limits &&
                                           undefined != e.Spec.Resources.Limits.MemoryBytes)
                              .reduce((a, b) => a + b.Spec.Resources.Limits.MemoryBytes, 0);
    let serviceLimitNanoCpu = serviceTasksDesired
                              .filter(e => undefined != e.Spec.Resources &&
                                           undefined != e.Spec.Resources.Limits &&
                                           undefined != e.Spec.Resources.Limits.NanoCPUs)
                              .reduce((a, b) => a + b.Spec.Resources.Limits.NanoCPUs, 0);

    metrics = addMetric(metrics, "docker_services_tasks_running", "Number of running tasks.", "counter");
    metrics = addRow(metrics, "docker_services_tasks_running", {
      "labels": metricLabels,
      "value": serviceTasksRunning.length
    });
    metrics = addMetric(metrics, "docker_services_tasks_desired", "Number of desired tasks.", "counter");
    metrics = addRow(metrics, "docker_services_tasks_desired", {
      "labels": metricLabels,
      "value": serviceTasksDesired.length
    });
    metrics = addMetric(metrics, "docker_services_reserved_memory_bytes", "Ram reserved for service.", "counter");
    metrics = addRow(metrics, "docker_services_reserved_memory_bytes", {
      "labels": metricLabels,
      "value": serviceReservedMemory
    });
    metrics = addMetric(metrics, "docker_services_reserved_nanocpu", "Cpu reserved for service.", "counter");
    metrics = addRow(metrics, "docker_services_reserved_nanocpu", {
      "labels": metricLabels,
      "value": serviceReservedNanoCpu
    });
    metrics = addMetric(metrics, "docker_services_limit_memory_bytes", "Ram limit for service.", "counter");
    metrics = addRow(metrics, "docker_services_limit_memory_bytes", {
      "labels": metricLabels,
      "value": serviceLimitMemory
    });
    metrics = addMetric(metrics, "docker_services_limit_nanocpu", "Cpu limit for service.", "counter");
    metrics = addRow(metrics, "docker_services_limit_nanocpu", {
      "labels": metricLabels,
      "value": serviceLimitNanoCpu
    });
  }
  return metrics;
}

function addPluginMetrics(metrics, plugins) {
  metrics = addMetric(metrics, "docker_plugins_installed_count", "Plugins installed on docker engine.", "counter");
  metrics = addRow(metrics, "docker_plugins_installed_count", {
    "labels": {},
    "value": plugins.length
  });

  metrics = addMetric(metrics, "docker_plugins_installed", "Plugins installed on docker engine.", "counter");
  for (var i in plugins) {
    metrics = addRow(metrics, "docker_plugins_installed", {
      "labels": {
        "plugin_name": plugins[i].Name
      },
      "value": 1
    });
  }
  return metrics;
}

function getRawData(metrics) {
  let data = [];
  for (metricName in metrics) {
    let m = metrics[metricName];
    data.push("# HELP " + metricName + " " + m.help);
    data.push("# TYPE " + metricName + " " + m.type);
    for (var row in m.rows) {
      let r = m.rows[row];
      let labels = "";
      if (Object.keys(r.labels).length > 0) {
        labels = "{" + renderLabels(r.labels) + "}";
      }
      data.push(metricName + labels + " " + r.value);
    }
  }
  return data;
}

function renderLabels(labels) {
  let all = [];
  for (let k in labels) {
    all.push(k + "=\"" + labels[k] + "\"");
  }
  return all.join(",");
}

function prefixKeys(prefix, object) {
  return Object.keys(object).reduce(function (result, key) {
    result[prefix + key.replace(/\./g , "_")] = object[key];
    return result;
  }, {});
}

