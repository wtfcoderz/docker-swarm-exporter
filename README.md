# Just an other prometheus exporter

## What metrics are exported ?

- Swarm Services Counter
- Swarm Task Status by Service
- ...

## How to test it

On a docker swarm mode node, do:

```bash
docker run -d \
--name docker-swarm-exporter \
-p 8080:8080 \
-v /var/run/docker.sock:/var/run/docker.sock:ro \
wtfcoderz/docker-swarm-exporter
```

Then you can curl metrcis like that:
```bash
curl http://127.0.0.1:8080/metrics
```

Just configure your prometheus to scrap this URL

## TODO
- Add many many metrics
- Give some grafana dashboards