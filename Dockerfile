FROM    node:9-alpine
WORKDIR /src
COPY    app.js /src
EXPOSE  8080
CMD     ["node", "app.js"]
